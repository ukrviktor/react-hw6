import { createAsyncThunk } from '@reduxjs/toolkit';
import { toggleFavorites } from '../reducers/productsThunk';

export const handleFavoritesClick = createAsyncThunk(
  'products/handleFavoritesClick',
  async (idFavorite, { getState, dispatch, rejectWithValue }) => {
    try {
      dispatch(toggleFavorites(idFavorite));
      // set the favorites when it changes
      const { products } = getState();
      localStorage.setItem('favorites', JSON.stringify(products.favorites));
      // return true;
    } catch (e) {
      return rejectWithValue(e.message);
    }
  }
);
